#include <stdio.h>
#include <time.h>

int main(){

	clock_t start = clock();

	FILE *fp;
	fp = fopen("/home/adam/Documents/WS4L_tests/one_gb.txt", "r");
	int sum = 0;
	int next = 0;
	while(fscanf(fp, "%d\n", &next) != EOF){
		sum += next; // will overflow, but not a problem;
	}
	printf("%015d\n", sum);
	fclose(fp);

	clock_t stop = clock();
	double secs = (double)(stop - start) / CLOCKS_PER_SEC;
	printf("%08.2f\n", secs);

	return 0;
}
