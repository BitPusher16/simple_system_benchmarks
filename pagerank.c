#include <stdio.h>
#include <time.h>

// http://www.cs.princeton.edu/~chazelle/courses/BIB/pagerank.htm
// http://www4.ncsu.edu/~ipsen/ps/slides_dagstuhl07071.pdf

// linear congruential generator;
// https://en.wikipedia.org/wiki/Linear_congruential_generator
unsigned int LCG(unsigned int seed){
	return (1140671485  * seed + 12820163 ) % 16777216;
}

int main(){

	clock_t start = clock();

	int m = 800;
	int iter = 20;
	int i, j, k;
	double pr;

	double A[m][m];
	double x[m];
	double b[m];
	double out[m];
	double d = .85;

	// populate adjacency matrix;
	unsigned int seed = 1337;
	for(i = 0; i < m; i++){
		for(j = 0; j < m; j++){
			seed = LCG(seed);
			A[i][j] = (double)((int)(seed >> 4) % 2);
		}
	}

	// compute outdegree of each node;
	// follow the convention that if element i,j = 1,
	// then there is an edge from i to j;
	for(i = 0; i < m; i++){
		double out_deg = 0;
		for(j = 0; j < m; j++){
			if(A[i][j] == 1.0){
				out_deg += 1;
			}
		}
		out[i] = out_deg;
	}

	// initialize pagerank vector to zeros;
	for(i = 0; i < m; i++){
		x[i] = 0.0;
	}

	// compute pagerank;
	for(i = 0; i < m; i++){
		x[i] = 0.0;
	}

	for(k = 0; k < iter; k++){
		for(i = 0; i < m; i++){
			// compute new pagerank for node i;
			pr = 0.0;
			for(j = 0; j < m; j++){
				// if there is an edge from node j to node i:
				if(A[j][i] == 1.0){
					pr += x[j] / out[j];
				}
			}
			pr = (1.0 - d) + d * pr;
			b[i] = pr;
		}

		// copy b to pagerank vector;
		for(i = 0; i < m; i++){
			x[i] = b[i];
		}
	} // end iterations;

	if(1 == 0){
		// print adjacency matrix;
		for(i = 0; i < m; i++){
			for(j = 0; j < m; j++){
				printf("%0.1f ", A[i][j]);
			}
			printf("\n");
		}

		// print outdegrees;
		for(i = 0; i < m; i++){
			printf("%0.1f\n", out[i]);
		}

		// print final pagerank vector;
		for(i = 0; i < m; i++){
			printf("%0.8f\n", x[i]);
		}
	}

	// print last 8 pagerank values;
	for(i = 0; i < 8; i++){
		printf("%0.8f\n", x[i]);
	}

	clock_t stop = clock();
	double secs = (double)(stop - start) / CLOCKS_PER_SEC;
	printf("%08.2f\n", secs);

	return 0;
}
