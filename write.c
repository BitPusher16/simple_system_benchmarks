#include <stdio.h>
#include <time.h>

int main(){

	clock_t start = clock();

	FILE *fp;
	fp = fopen("/home/adam/Documents/WS4L_tests/one_gb.txt", "w");
	int i = 0;
	for(i = 0; i < 268435456; i++){ // 4GB
		fprintf(fp, "%015d\n", i);
	}
	fclose(fp);

	clock_t stop = clock();
	double secs = (double)(stop - start) / CLOCKS_PER_SEC;
	printf("%08.2f\n", secs);

	return 0;
}
