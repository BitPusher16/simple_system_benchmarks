#include <stdio.h>
#include <time.h>

// linear congruential generator;
// https://en.wikipedia.org/wiki/Linear_congruential_generator
unsigned int LCG(unsigned int seed){
	return (1140671485  * seed + 12820163 ) % 16777216;
}

int main(){

	clock_t start = clock();

	int m = 200;
	int iter = 1600;
	int i, j, k, p;
	double num, den, dot;
	unsigned int seed = 1337;

	double A[m][m];
	double B[m][m];
	double C[m][m];

	for(p = 0; p < iter; p++){
		// populate A and B;
		for(i = 0; i < m; i++){
			for(j = 0; j < m; j++){
				seed = LCG(seed);
				num = (int)seed;
				seed = LCG(seed);
				den = (int)seed;
				A[i][j] = num / (den + 0.000001);
				seed = LCG(seed);
				num = (int)seed;
				seed = LCG(seed);
				den = (int)seed;
				B[i][j] = num / (den + 0.000001);
			}
		}

		// compute C;
		for(i = 0; i < m; i++){
			for(j = 0; j < m; j++){
				dot = 0;
				for(k = 0; k < m; k++){
					dot += A[i][k] * B[k][j];
				}
				C[i][j] = dot;
			}
		}
	}

	clock_t stop = clock();
	double secs = (double)(stop - start) / CLOCKS_PER_SEC;
	printf("%08.2f\n", secs);

	return 0;
}
